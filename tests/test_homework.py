import time
import pytest

from pages.arena.add_project_page import project_name

from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver import Chrome

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from pages.arena.home_page import HomePage
from pages.arena.login_page import LoginPage
from pages.arena.administration_page import AdminPage
from pages.arena.add_project_page import AddProjectPage
from pages.arena.projects_page import ProjectsPage

# ZMIENNE GLOBALNE
url = "http://demo.testarena.pl/zaloguj"
email_login = "administrator@testarena.pl"
password_login = "sumXQQ72$L"


@pytest.fixture
def browser():
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1680, 1050)

    login_page = LoginPage(browser)
    login_page.visit(url)
    login_page.login(email_login, password_login)

    yield browser

    browser.quit()


def test_add_new_project_and_verification(browser):
    # LOGOWANIE
    user_email = browser.find_element(By.CSS_SELECTOR, ".user-info small").text
    assert browser.find_element(By.CSS_SELECTOR, "a[title='Wyloguj']").is_displayed()
    assert user_email == email_login

    # OTWÓRZ PANEL ADMINA
    home_page = HomePage(browser)
    home_page.click_on_administration_link()
    assert "administration" in browser.current_url

    # OTWÓRZ STRONĘ DODAWANIA NOWEGO PROJEKT
    admin_page = AdminPage(browser)
    admin_page.click_add_project()
    assert browser.find_element(By.CLASS_NAME, "content_title").text == "Dodaj projekt"

    # DODAJ NOWY PROJEKT
    add_project_page = AddProjectPage(browser)
    add_project_page.add_project()
    wait = WebDriverWait(browser, 10)
    project_add_info_box = (By.CSS_SELECTOR, "#j_info_box p")
    wait.until(expected_conditions.visibility_of_element_located(project_add_info_box))
    assert browser.find_element(By.CSS_SELECTOR, "#j_info_box p").text == "Projekt został dodany."
    assert browser.find_element(By.CLASS_NAME, "content_label_title").text == project_name

    # zapisz url projektu
    url_project = browser.current_url

    # PRZEJDŹ DO SEKCJI 'PROJEKTY'
    add_project_page.go_to_projects_page()
    assert "projects" in browser.current_url
    assert browser.find_element(By.CLASS_NAME, "content_title").text == "Projekty"

    # WYSZUKAJ NOWO UTWORZONY PROJEKT PO NAZWIE
    projects_page = ProjectsPage(browser)
    projects_page.search_projects(project_name)
    search_projects_name = browser.find_elements(By.CSS_SELECTOR, 'tbody tr td:nth-of-type(1)')
    for name in search_projects_name:
        if name.text == project_name:
            assert name.text == project_name

    # WEJDŹ DO WŁAŚCIWOŚCI PROJEKTU
    projects_page.view_project_properties(url_project)
    assert browser.find_element(By.CLASS_NAME, "content_label_title").text == project_name