from selenium.webdriver.common.by import By

logout_icon = ".header_logout"
administrator_icon = ".header_admin a"

class HomePage:
    def __init__(self, browser):
        self.browser = browser

    def click_on_administration_link(self):
        self.browser.find_element(By.CSS_SELECTOR, administrator_icon).click()

