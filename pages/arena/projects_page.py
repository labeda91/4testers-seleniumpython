from selenium.webdriver.common.by import By


class ProjectsPage:
    def __init__(self, browser):
        self.browser = browser

    def search_projects(self, project_name):
        self.browser.find_element(By.CSS_SELECTOR, '#search').send_keys(project_name)
        self.browser.find_element(By.CSS_SELECTOR, '#j_searchButton').click()

    def view_project_properties(self, url_project):
        self.browser.find_element(By.CSS_SELECTOR, f"[href='{url_project}']").click()
