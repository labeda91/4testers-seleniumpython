import string
import random
import lorem

from selenium.webdriver.common.by import By


def get_random_string_letter(length):
    return "".join(random.choices(string.ascii_uppercase + string.digits, k=length))


def get_random_string_number(length):
    return "".join(random.choices(string.digits, k=length))


project_name = get_random_string_letter(10)
prefix = get_random_string_number(6)
description = lorem.paragraph()


class AddProjectPage:
    def __init__(self, browser):
        self.browser = browser

    def add_project(self):
        self.browser.find_element(By.CSS_SELECTOR, "#name").send_keys(project_name)
        self.browser.find_element(By.CSS_SELECTOR, "#prefix").send_keys(prefix)
        self.browser.find_element(By.CSS_SELECTOR, "#description").send_keys(description)
        self.browser.find_element(By.CSS_SELECTOR, "#save").click()

    def go_to_projects_page(self):
        self.browser.find_element(By.CLASS_NAME, "activeMenu").click()
