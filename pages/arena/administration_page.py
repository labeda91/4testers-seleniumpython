from selenium.webdriver.common.by import By


class AdminPage:
    def __init__(self, browser):
        self.browser = browser

    def click_add_project(self):
        self.browser.find_element(By.CSS_SELECTOR, "[href='http://demo.testarena.pl/administration/add_project']").click()
