from selenium.webdriver.common.by import By


class LoginPage:
    def __init__(self, browser):
        self.browser = browser

    # otwarcie strony
    def visit(self, url):
        self.browser.get(url)

    def login(self, email_login, password_login):
        self.browser.find_element(By.CSS_SELECTOR, "#email").send_keys(email_login)
        self.browser.find_element(By.CSS_SELECTOR, "#password").send_keys(password_login)
        self.browser.find_element(By.CSS_SELECTOR, "#login").click()
